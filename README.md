# Itelios Branch Cleaner
Tool of mass branch deletion

## Introduction
### What am I looking at ?
The Branch Cleaner is a tool made for delete multiple branches at once on a git project. To help you do it, a CLI will guide you through the steps.

### How do I use it, the quick way ?
Clone the Branch Cleaner repository anywhere on your machine.
`npm install` the dependencies.
Start the CLI with `npm start`
Follow the steps

### Is it dangerous to use it ?
**Yes**, caution is advised.
Deletions cannot be undone, but you'll obviously get a preview of the change before you can delete anything.

### On what basis are branch deleted ?
Before deleting anything, you will have to create a filter. The filter follow the following logic :
For each branches on the remote, we ask :
Is this branch too old ? Does it has some non-merged commit (and does it matter) ? Is this branch part of the one we should ignore ?

## Usage

### Prequisite
You need to have a local clone of the repository you want to work on. You don't have to set it up thought, it just have to be there.
You'll also need to have set your local global git configuration so you have an access (and the right to delete) to the repository 
You also need Node.js installed on your machine

### Starting the Branch Cleaner
To launch the tool, launch a command line in the root of the installation folder. There, install the dependencies with the command `npm install`, and then `npm start` (or `node index.js`)
At this point the CLI starts, and you can follow the instruction. You can read details about those steps below.

### Steps
The CLI first ask a serie of three questions
 - First you have to enter the path to the local clone of the repo you want to work on. It can be an absolute or relative path.
 - Then you have to give the name of the remote of the reository. Odds are it has the default name "origin".
 - Finally you have to give the name of the branch you consider as the main branch. This branch will be use to calculate wether other branches have some ahead (non-merged commits) or not.

You then land on the main menu :

**Create a filter**
To select branch you need a filter. The interface will ask a set of three questions to build one, and will then show you what would happen if you were to apply it
 - First you'll have to enter a number of days. Any branch on which the last commit is older than this number of days will be considered old and checked with further tests, otherwise, it will be considered valid and will not be deleted.
 - Then you have to decide wether or not non-merged commit are relevant or not. If you select "Yes" aheads will be calculated for old branches, and those with non-merged commit will be ignored.
  - Lastly, you will be invited to enter branch names. Any branch matching the pattern {remote_name}/{name} will be ignored

At this point, a simulation of the filter is shown. Green branch will be kept, red one will be deleted (for colorblind people, a blue "*" before the name means it would be kept, a yellow "-" means it would be destroyed).
The reason of the decision is also displayed allowing you to inderstand it and mprove your filter if needed.
If you project contains a lot of branch, there is a possibility you command line won't show you every branch. You'll have to increase the size of the line buffer of you command line

**Test my filter**
Print a simulation of your filter if you already created one
Though it is redundant with the output you obtained from the filter creation, it will take in account change you could have made on the repo (such as a merge or a commit) since the first output

**Apply my filter**
After a confirmation, apply the filter.
You can stop the program at any time, but what is deleted is gone forever.

**Leave**
The program exists

## Configuration
You can easily add names in the file "config.json"
As the name are used inside the constructor of a new Regex, you are free to be creative.
