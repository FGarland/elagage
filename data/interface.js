const textAndQuestions = {

    introduction: "A local repository is necessary to access the remote assiocated with it. If you don't have a local copy, you'll have to clone the repository on your device.\n\
Take note that your git global configuration will be used to push change on the remote, please make sure the set credentials grant you the needed access and rights",

    pathQuestion: {
        type: 'input',
        name: 'path',
        message: "Please enter the path (absolute or relative) to the local repository you want to clean :\n"
    },
    originQuestion: {
        type: 'input',
        name: 'origin',
        message: "Please enter the name of the repository's origin name :\n",
        default: 'origin'
    },

    mainBranchQuestion: {
        type: 'input',
        name: 'mainBranch',
        message: "Please enter the repository's main branch. This branch will be used to calculate aheads an behinds.\n",
        default: 'master'
    },

    mainMenuQuestion: {
        type: 'list',
        name: 'mainMenuChoice',
        message: 'What do want to do ?\n',
        choices: [
            'Create a filter',
            'Test my filter',
            'Apply my filter',
            'Leave'
        ]
    },

    createFilterQuestionDate: {
        type: 'input',
        name: 'date',
        message: 'Please enter a number of days. Any branch on which the last commit is older will be considerer old, and will be considered for deletion :\n'
    },

    createFilterQuestionKeepAhead: {
        type: 'list',
        name: 'keepAhead',
        message: 'Do you want to keep the branches with non merged commits (ahead compared to the given main branch) ?\n',
        choices: [
            'Yes',
            'No'
        ]
    },

    createFilterQuestionNames: {
        type: 'input',
        name: 'names',
        message: "If you want to keep certain branches/folder, please enter their name here. \
Any branch containing these words will be kept.\n\
You can see a list of default name (and expand it) in the file config.json \n\
Enter the names separated with spaces (this field can be left empty) :\n"
    },

    deletionConfirmationQuestion: {
        type: 'list',
        name: 'delete',
        message: "Are you sure ?",
        choices: [
            'Yes',
            'No'
        ]
    }
}

module.exports = textAndQuestions