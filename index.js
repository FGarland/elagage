const inquirer = require('inquirer')
const chalk = require('chalk')
const BranchManager = require('./model/BranchManager')
const Repository = require('./model/Repository')
const interfaceData = require('./data/interface')

function initQuestionPath(repository){
    inquirer.prompt(interfaceData.pathQuestion).then(async answer => {
        let res = answer.path.trim()
        if(res.match(/(^[A-Z]:).*|(^\.).*/)){
            let result = !! await Repository.testRepo(answer.path)
            if(!result){
                console.log(chalk.redBright(`Are you sure a repo is set at the end of this path ?`))
                initQuestionPath(repository)
            } else {
                repository.path = answer.path
                initQuestionOrigin(repository)
            }
        } else {
            console.log(chalk.redBright(`This does not look like a path`))
            initQuestionPath(repository)
        }
    })
}

function initQuestionOrigin(repository){
    inquirer.prompt(interfaceData.originQuestion).then(async answer => {
        let res = answer.origin.trim()
        if(res.split(' ').length === 1){
            let result = await Repository.testOrigin(res)
            if(!result){
                console.log(chalk.redBright(`There is apparently no remote named as such`))
                initQuestionOrigin(repository)
            } else {
                repository.origin = res
                initQuestionMainBranch(repository)
            }
        } else {
            console.log(chalk.redBright(`This does not look like a real name`))
            initQuestionOrigin(repository)
        }
    })
}

function initQuestionMainBranch(repository){
    inquirer.prompt(interfaceData.mainBranchQuestion).then(async answer => {
        let res = answer.mainBranch.trim()
        if(res.split(' ').length === 1){
            let result = await Repository.testMainBranch(res, repository.origin, repository.path)
            if(result){
                repository.mainBranch = res
                branchManager = new BranchManager(repository)
                await branchManager.loadBranches()
                branchManager.printSummary()
                mainMenu()
            } else {
                console.log(chalk.redBright(`It appears there is no such branch on the remote`))
                initQuestionMainBranch(repository)
            }
        } else {
            console.log(chalk.redBright(`There was some space in your name`))
            initQuestionMainBranch(repository)
        }
    })
}

function mainMenu(){
    inquirer.prompt(interfaceData.mainMenuQuestion).then(async answer => {
        switch(answer.mainMenuChoice){
            case 'Create a filter':
                createFilterDate()
                break;
            case 'Test my filter':
                if(currentFilter.date){
                    await branchManager.previewDeletion(currentFilter)
                    mainMenu()
                } else {
                    console.log(chalk.red(`You don't have a filter yet, please create one`))
                    mainMenu()
                }
                break;
            case 'Apply my filter':
                if(currentFilter.date){
                    deletionConfirmation()
                } else {
                    console.log(chalk.red(`You don't have a filter yet, please create one`))
                    mainMenu()
                }
                break;
            case 'Leave':
                console.log(`Good Bye`)
                break;
            default : 
                console.log(chalk.red(`I'm sorry, what ?`))
        }
    })
}

function createFilterDate(){
    inquirer.prompt(interfaceData.createFilterQuestionDate).then(answer => {
        let res = parseInt(answer.date.trim(),10)
        if(!isNaN(res)){
            currentFilter.date = new Date(Date.now() - res * 24*3600000)
            console.log(chalk.blueBright(`That brings us to the ${currentFilter.date.toDateString()}`))
            createFilterKeepAhead()
        } else {
            console.log(chalk.redBright(`This date is apparently not correctly formatted`))
            createFilterDate()
        }
    })
}

function createFilterKeepAhead(){
    inquirer.prompt(interfaceData.createFilterQuestionKeepAhead).then(answer => {
        currentFilter.keepAhead = answer.keepAhead === 'Yes' ? true : false
        createFilterNames()
    })
}

function createFilterNames(){
    inquirer.prompt(interfaceData.createFilterQuestionNames).then(async answer => {
        currentFilter.names = answer.names.trim().split(' ').filter(e => e.length > 0)
        await branchManager.previewDeletion(currentFilter)
        mainMenu()
    })
}

function deletionConfirmation(){
    inquirer.prompt(interfaceData.deletionConfirmationQuestion).then(async answer => {
        if(answer.delete === "Yes"){
            await branchManager.deleteBranchesMatchingFilter(currentFilter)
            mainMenu()
        } else {
            mainMenu()
        }
    })
}

let branchManager
let repository = new Repository
let currentFilter = {
    date: '',
    keepAhead: '',
    names: ''
}
console.log(interfaceData.introduction)
initQuestionPath(repository)
