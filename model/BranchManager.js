const simpleGit = require('simple-git/promise');
const Branch = require('./Branch')
const chalk = require('chalk')
const configJSON = require('../config.json')

/**
 * A standard forEach become naturally asynchronous if the given function return a promise.
 * To keep things synchronous we use this for loop
 * @param {*} array
 * @param {*} callback
 */
async function asyncForEach(array, callback) {
    for (let index = 0; index < array.length; index++) {
      await callback(array[index], index, array);
    }
  }

  class BranchManager {

    constructor(repository){
        this.repository = repository
        this.branchList = {}
    }

    /**
     * Retrive a list of branch from designated remote, and parse it in a object
     * and assign it to this.branchList
     * the retrived list is a string looking like this :
     *     "1572968545 +0100 origin/HEAD
     *      1572968545 +0100 origin/master
     *      (...)
     *      1574153819 +0100 origin/long/branch/name
     *      "
     *
     * @memberof BranchManager
     */
    async loadBranches(){
        let rawResponse = await simpleGit(this.repository.path).raw(['branch', '-r', '--list', this.repository.origin + '/*', '--format', '%(authordate:raw) %(refname:short)'])
            .catch(error => console.log(`Error while fetching branches : ${error}`))
        let branchLines = rawResponse.split('\n')
        if(branchLines[branchLines.length - 1] === ''){ branchLines.pop() } //erase newline a end of response
        this.branchList = {}
        branchLines.forEach(line => {
            let branchDetails = line.split(' ')
            this.branchList[branchDetails[2]] = new Branch(branchDetails[2], branchDetails[0], this.repository)
        });
    }

    async previewDeletion(filter){
        console.log(`Please wait...`)
        let result = await this.applyFilter(filter)
            .catch(e => {
                console.log(`Error while applying filters : ${e}`)
            })
        result.keepingList.forEach(branch => {
            console.log(this.branchList[branch].summary)
        })
        result.deletionList.forEach(branch => {
            console.log(this.branchList[branch].summary)
        })
        console.log(`Number of branches to delete : ${result.deletionList.length}/${Object.keys(this.branchList).length} (${result.keepingList.length} branches to keep)`)
    }

    async deleteBranchesMatchingFilter(filter){
        let result = await this.applyFilter(filter)
            .catch(e => {
                console.log(`Error while applying filters : ${e}`)
            })
        await asyncForEach(result.deletionList, async branch => {
            let result = await this.branchList[branch].delete()
            if(result === '') {
                console.log(`${branch} deleted`)
                delete this.branchList[branch]
            } else {
                console.log(result)
            }
        })
        console.log(chalk.greenBright("The deletion is over"))
        return true;
    }

    async applyFilter(filter){
        let names = filter.names.concat(configJSON["protected-names"])
        let keepingList = []
        let deletionList = []
        let counter = 0
        let branchCount = Object.keys(this.branchList).length
        await asyncForEach(Object.values(this.branchList), async branch => {
            process.stdout.clearLine();
            process.stdout.cursorTo(0);
            process.stdout.write(`${counter++}/${branchCount}`);
            let filterResult = await branch.isFiltered(filter.date, filter.keepAhead, names)
                .catch(error => {
                    console.log(`Error in Branch::isFiltered : ${error}`)
                })
            if(filterResult){
                deletionList.push(branch.name)
            } else {
                keepingList.push(branch.name)
            }
        })
        process.stdout.clearLine();
        process.stdout.cursorTo(0);
        return { keepingList, deletionList }
    }

    printSummary(){
        console.log(chalk.blueBright(`${Object.keys(this.branchList).length} branches detected`))
    }

}

module.exports = BranchManager