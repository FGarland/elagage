const simpleGit = require('simple-git/promise');

class Repository {
    constructor(path, origin, mainBranch){
        this.path = path
        this.origin = origin
        this.mainBranch = mainBranch
    }

    static async testRepo(path){
        let result
        result = !! await simpleGit(path).silent(true).branch().catch(() => result = false)
        return result
    }
    
    static async testOrigin(origin){
        let result
        let rawResult = await simpleGit(this.path).raw(['remote']).catch(() => result = false)
        if(rawResult !== false) {
            rawResult = rawResult.split('\n').filter(e => e !== '')
            result = rawResult.includes(origin) ? true : false
        }
        return result
    }
    
    static async testMainBranch(mainBranch, origin, path){
        let result
        result = !! await simpleGit(path).raw(['ls-remote', '--heads', origin, mainBranch]).catch(() => result = false)
        return result
    }
}
module.exports = Repository