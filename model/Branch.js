const simpleGit = require('simple-git/promise');
const chalk = require('chalk')

class Branch {

    constructor(name, lastCommitDate, repository) {
        this.name = name
        this.lastCommitDate = lastCommitDate * 1000
        this.repository = repository
    }

    async fetchBehindAhead(){
        let rawBA = await simpleGit(this.repository.path).raw([
            'rev-list',
            '--left-right',
            '--count',
            `${this.repository.origin}/${this.repository.mainBranch}...${this.name}`])
                .catch(error => {
                    console.log(`Error in function getAheadBehind : ${error}`);
                })
        let behindAheadArray = rawBA.replace('\n', '').split('\t')
        this.behind = parseInt(behindAheadArray[0], 10)
        this.ahead = parseInt(behindAheadArray[1], 10)
        return this
    }

    async delete(){
        let name = this.name.replace(this.repository.origin + '/', '')
        let result
        result = await simpleGit(this.repository.path).silent(true).push(this.repository.origin, name, {'--delete': null})
            .catch(error => result = `Error while deleting ${this.name} : ${error}`)
        return result
    }

    isTooOld(date){
        return Date.parse(date) > this.lastCommitDate
    }

    async hasAhead(){
        if(!this.ahead){
            await this.fetchBehindAhead()
        }
        return this.ahead > 0
    }

    /**
     * Return true if this branch name match one of the protected pattern (such as "release")
     * List of protected pattern can be completed in config.json
     * @param {Array<String>} names
     * @returns {Boolean}
     * @memberof Branch
     */
    isProtectedByName(names){
        let protectedName = false
        names.forEach(name => {
            let pattern = new RegExp(`^${this.repository.origin}/${name}`)
            if(this.name.match(pattern)){
                protectedName = true
            }
        });
        return protectedName
    }

    async isFiltered(date, keepAheadBranch, names){
        if(this.isProtectedByName(names)){
            this.renderSummary(false, chalk.green('protected by name'))
            return false
        } else {
            if (this.isTooOld(date)){
                if (keepAheadBranch){
                    let hasAhead = await this.hasAhead().catch(e => console.log(e))
                    let reason = hasAhead ? chalk.green(`too old, but has ${this.ahead} commit(s) ahead`) : chalk.red('too old, has no ahead ')
                    this.renderSummary(!hasAhead, reason)
                    return !hasAhead
                } else {
                    this.renderSummary(true, chalk.red('is too old'))
                    return true
                }
            } else {
                this.renderSummary(false, chalk.green('young branch'))
                return false
            }
        }
    }

    renderSummary(isOnDeletionList, reason){
        let symbol = isOnDeletionList ? chalk.yellow('- ') :  chalk.blueBright('* ')
        let coloredName = isOnDeletionList ? chalk.redBright(this.name) : chalk.green(this.name)
        this.summary = symbol + reason.padEnd(50, ' ') + chalk.blueBright(new Date(this.lastCommitDate).toLocaleDateString().padEnd(12, ' ')) + coloredName
    }
}


module.exports = Branch